const fsPromises = require('fs/promises')

function assignByKey(target, path, value) {
    var section = target;

    const keys = path.split(".");
    for(let i = 0; i < keys.length - 1; i++) {
        let key = keys[i];
        section = section[key];
    }
    section[keys[keys.length - 1]] = value;
    return target;
}

module.exports = {
    onPreBuild: async ({ inputs }) => {
        const {configFilePath, configSettingPath, environmentSource, valueMaxLength} = inputs;
        let targetValue = process.env[environmentSource].substring(0, valueMaxLength);

        console.debug("Target source: ", environmentSource);
        console.debug("Target value: ", targetValue);
        console.debug("Target file: ", configFilePath);
        console.debug("Target targetKey: ", configSettingPath);

        const file = await fsPromises.readFile(configFilePath);
        const config = assignByKey(JSON.parse(file), configSettingPath, targetValue);

        console.log("Updated config: ", config);
        await fsPromises.writeFile(configFilePath, JSON.stringify(config));
    }
}