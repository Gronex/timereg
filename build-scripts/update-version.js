const fs = require('fs/promises')

function assignByKey(target, path, value) {
    var section = target;

    const keys = path.split(".");
    for(let i = 0; i < keys.length - 1; i++) {
        let key = keys[i];
        section = section[key];
    }
    section[keys[keys.length - 1]] = value;
    return target;
}

async function updateVersion(configFilePath, configSettingPath, value) {
    console.debug("Target value: ", value);
    console.debug("Target file: ", configFilePath);
    console.debug("Target targetKey: ", configSettingPath);

    const file = await fs.readFile(configFilePath);
    const config = assignByKey(JSON.parse(file), configSettingPath, value);

    console.log("Updated config: ", config);
    await fs.writeFile(configFilePath, JSON.stringify(config));
}

updateVersion(process.argv[2], "Version.CommitMeta", process.argv[3]);