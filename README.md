# Timereg

# Getting started:

## .NET

1. Install .NET 8 SDK
2. Run solution using `Gronia.Timereg.Client` run profile

## JS

1. Install Volta https://docs.volta.sh
2. `npm ci`
3. `npm run build:all`