using System;

using Gronia.Timereg.Domain;

namespace Gronia.Timereg.Client.ViewModels
{
    public record TimeRegistrationViewModel
    {
        public Guid Id { get; init; }

        public DateOnly Date { get; set; }

        public string Project { get; set; }

        public string Description { get; set; }

        public TimeOnly StartTime { get; set; }

        public TimeOnly? StopTime { get; set; }

        public TimeOnly Time
        {
            get
            {
                TimeSpan? diff = (StopTime - StartTime);
                return diff < TimeSpan.Zero ? TimeOnly.MinValue : TimeOnly.FromTimeSpan(diff ?? TimeSpan.Zero);
            }
            set
            {
                StartTime = TimeOnly.MinValue;
                StopTime = value == TimeOnly.MinValue ? null : value;
            }
        }

        // TODO: Mapping should probably be its own thing
        public TimeRegistrationViewModel(TimeRegistration timeRegistration)
        {
            Id = timeRegistration.Id;
            Date = timeRegistration.Date;
            Project = timeRegistration.Project;
            Description = timeRegistration.Description;
            StartTime = timeRegistration.StartTime;
            StopTime = timeRegistration.StopTime;

        }

        public TimeRegistrationViewModel()
        {
            Project = string.Empty;
            Description = string.Empty;
        }

        public TimeRegistration ToDomainModel()
        {
            return new TimeRegistration
            {
                Date = Date,
                Description = Description,
                Project = Project,
                StartTime = StartTime,
                StopTime = StopTime
        };
        }
    }
}
