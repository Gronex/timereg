﻿using System;

namespace Gronia.Timereg.Client.Options
{
    public class VersionOptions
    {
        public const string Key = "Version";

        public string CommitMeta { get; set; } = "Local";
    }
}
