﻿using Gronia.Timereg.Client.Pages;

namespace Gronia.Timereg.Client;

[Routing.RouteProvider(typeof(Day))]
[Routing.RouteProvider(typeof(Edit))]
[Routing.RouteProvider(typeof(Home))]
[Routing.RouteProvider(typeof(Settings))]
public partial class RoutingService
{
}
