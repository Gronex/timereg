// This is a JavaScript module that is loaded on demand. It can export any number of
// functions, and may import other JavaScript modules if required.

export async function downloadFile(stream, name, mimeType) {
    const arrayBuffer = await stream.arrayBuffer()

    const file = new File([arrayBuffer], name, { type: mimeType })

    const link = document.createElement('a');
    link.download = name;
    link.href = URL.createObjectURL(file);
    link.click();
    link.remove();
    URL.revokeObjectURL(file);
}
