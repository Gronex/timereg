using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

// https://sourcegen.dev/ to test
namespace Gronia.Timereg.RoutingGenerator
{
    [Generator(LanguageNames.CSharp)]
    public class RouteGenerator : IIncrementalGenerator
    {

        public void Initialize(IncrementalGeneratorInitializationContext context)
        {
            context.RegisterPostInitializationOutput(static postInitializationContext =>
                postInitializationContext.AddSource("RoutingAttribute.cs", SourceText.From("""
                    using System;
                    namespace Gronia.Timereg.Routing
                    {

                        [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
                        internal sealed class RouteProviderAttribute : Attribute
                        {
                            public RouteProviderAttribute(Type pageType)
                            {
                            }
                        }
                    }
                    """, Encoding.UTF8))
                );

            var pipeline = context.SyntaxProvider.ForAttributeWithMetadataName(
                fullyQualifiedMetadataName: "Gronia.Timereg.Routing.RouteProviderAttribute",
                predicate: (syntaxNode, cancellationToken) => syntaxNode.IsKind(SyntaxKind.ClassDeclaration),
                transform: (context, cancellationToken) =>
                {
                    var serviceNamespace = context.TargetSymbol.ContainingNamespace?.ToDisplayString(SymbolDisplayFormat.FullyQualifiedFormat.WithGlobalNamespaceStyle(SymbolDisplayGlobalNamespaceStyle.Omitted)) ?? "Generated";
                    var pages = context.Attributes.SelectMany(x => x.ConstructorArguments.Select(ca => ca.Value)).OfType<ITypeSymbol>();

                    return new Model(serviceNamespace, context.TargetSymbol.Name, pages);
                }
            );

            context.RegisterSourceOutput(pipeline, (context, model) =>
            {
                var builder = new StringBuilder();
                foreach(var page in model.TargetPageType)
                {
                    WriteMethod(page, builder);
                }

                var sourceText = $$"""
                using System;

                namespace {{model.Namespace}};
                public partial class {{model.ClassName}}
                {
                    {{builder}}
                }
                """;

                //Debugger.Launch();
                context.AddSource("RouteService.g.cs", SourceText.From(sourceText, Encoding.UTF8));
            });
        }

        private void WriteMethod(ITypeSymbol pageTypeSymbol, StringBuilder stringBuilder)
        {
            AttributeData? attribute = pageTypeSymbol?.GetAttributes().FirstOrDefault(attr => attr?.AttributeClass?.Name == "RouteAttribute");
            stringBuilder.AppendLine($"// {pageTypeSymbol?.MetadataName}");
            if (attribute != null && pageTypeSymbol != null)
            {
                AddRouteFunctions(pageTypeSymbol, attribute, stringBuilder);
            }
        }

        private void AddRouteFunctions(ITypeSymbol page, AttributeData attribute, StringBuilder stringBuilder)
        {
            var tokenRegex = new Regex(@"\{(?<replacementToken>.+?)\}");
            var template = attribute.ConstructorArguments.First().Value?.ToString() ?? string.Empty;

            MatchCollection? matches = tokenRegex.Matches(template);

            var args = new List<(string type, string param, bool optional)>();
            foreach (var match in matches)
            {
                foreach (var token in (match as Match)!.Groups["replacementToken"].Captures)
                {
                    (string TypeName, string ParameterName, bool Optional) parameter = GetParameter(token.ToString());
                    args.Add(parameter);
                    template = template.Replace(token.ToString(), RenderReplacementToken(parameter.TypeName, parameter.ParameterName, parameter.Optional));
                }
            }

            stringBuilder.AppendLine($$"""
            public string Get{{page.Name}}Route({{string.Join(", ", args.Select(x => RenderParameter(x.type, x.param, x.optional)))}})
            {
                return $"{{template}}";
            }
            """);
        }

        private string RenderReplacementToken(string type, string parameterName, bool optional)
        {
            if (optional)
            {
                type = type.TrimEnd('?');
            }

            string formatter = (type) switch
            {
                nameof(DateTime) => $$"""{{(optional ? "?" : string.Empty)}}.ToString("s", {{typeof(System.Globalization.CultureInfo).FullName}}.InvariantCulture)""",
                "DateOnly" => $$"""{{(optional ? "?" : string.Empty)}}.ToString("s", {{typeof(System.Globalization.CultureInfo).FullName}}.InvariantCulture)""",
                _ => string.Empty,
            };

            return $"{parameterName}{formatter}";
        }

        private string RenderParameter(string typeName, string parameterName, bool optional)
        {
            var param = $"{typeName} {parameterName}";
            if (optional)
            {
                param += " = null";
            }
            return param;
        }

        private (string TypeName, string ParameterName, bool Optional) GetParameter(string match)
        {
            var parts = match.Split(':');

            var type = parts.Length > 1 ? parts[1] : "string";

            bool optional = match.Contains('?');
            var trimmed = type.Replace("?", string.Empty);
            type = (trimmed) switch
            {
                "datetime" => nameof(DateTime),
                "dateonly" => "DateOnly",
                "guid" => nameof(Guid),
                _ => trimmed,
            };

            type += optional ? "?" : string.Empty;

            return (type, parts[0].Replace("?", string.Empty), optional);
        }

        private record Model
        {
            public Model(string @namespace, string className, IEnumerable<ITypeSymbol> targetPageType)
            {
                Namespace = @namespace;
                ClassName = className;
                TargetPageType = targetPageType;
            }

            public string Namespace { get; }

            public string ClassName { get; }

            public IEnumerable<ITypeSymbol> TargetPageType { get; set; }
        }
    }

    /// <summary>
    /// Created on demand before each generation pass
    /// </summary>
    class SyntaxReceiver : ISyntaxReceiver
    {
        public List<ClassDeclarationSyntax> CandidateClasses { get; } = new List<ClassDeclarationSyntax>();

        /// <summary>
        /// Called for every syntax node in the compilation, we can inspect the nodes and save any information useful for generation
        /// </summary>
        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            // any field with at least one attribute is a candidate for property generation
            if (syntaxNode is ClassDeclarationSyntax classDeclarationSyntax
                && classDeclarationSyntax.AttributeLists.Count > 0
                )
            {
                CandidateClasses.Add(classDeclarationSyntax);
            }
        }
    }
}
