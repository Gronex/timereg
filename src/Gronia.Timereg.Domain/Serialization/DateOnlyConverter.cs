﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Gronia.Timereg.Domain.Serialization
{
    public class DateOnlyConverter : JsonConverter<DateOnly>
    {
        public override DateOnly Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var str = reader.GetString();
            if (!DateTime.TryParse(str, out var value))
            {
                if(DateOnly.TryParse(str, out var dateOnly))
                {
                    return dateOnly;
                }
                else
                {
                    throw new NotSupportedException($"Cannot convert '{str}' to DateOnly");
                }
            }
            return DateOnly.FromDateTime(value);
        }

        public override void Write(Utf8JsonWriter writer, DateOnly value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString("o"));
        }
    }
}
