using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

using Gronia.Timereg.Domain.Serialization;


//using Gronia.Timereg.Domain.Serialization;
using Gronia.Timereg.IndexedDb;

namespace Gronia.Timereg.Domain
{
    public record TimeRegistration : IDbModel<Guid>
    {
        public Guid Id { get; init; }

        [JsonConverter(typeof(DateOnlyConverter))]
        public DateOnly Date { get; init; }

        public string Project { get; init; } = string.Empty;

        public string Description { get; init; } = string.Empty;

        [JsonConverter(typeof(TimeOnlyConverter))]
        public TimeOnly StartTime { get; init; }

        [JsonConverter(typeof(TimeOnlyConverter))]
        public TimeOnly? StopTime { get; init; }

    }
}
